Raphaël GLEIZE

# Présentation du projet de MMSE

Lien du sujet : https://gitlab.com/raphou66g/aspicproj


## Disclaimer

Il est possible que le projet final soit légèrement différent de ce qui est décrit ci-dessous.
Dans l'ensemble, le projet devrait rester fidèle à sa description.

## Présentation

Pour ce projet de MMSE, je suis parti sur l'idée d'un système de vérouillage/dévérouillage par NFC.

Ne possédant pas de module RFID/NFC dans mon kit Arduino, la lecture du badge se fera par Android.
Comme j'ai en ma posséssion un module Bluetooth pour Arduino, c'est donc par ce biai que se fera la communication.

Dans le détail, via une application, l'Android scannera le tag NFC et enverra un code d'authentification (son ID), par Bluetooth, à l'Arduino.
Cette dernière traitera ce code avec ceux en sa posséssion pour authoriser ou non le déverouillage.

De plus, il sera possible d'ajouter à la volée d'autres identifiants à la liste des codes autorisés. (1 master code + 4 dynamiques pour commencer)

Pour plus de lisibilité, un affichage LCD et/ou une led RGB pourra être ajouté pour un meilleur retour visuel.

## Matériel

La liste suivante peut être ammenée à changer entre maintenant et la réalisation finale :

- 1 smartphone Android
- 1 carte Arduino Uno
- 1 module Bluetooth
- 1 Servo Moteur
- 1 led RGB (Optionnel)
- 1 afficheur LCD (Optionnel)
- des fils