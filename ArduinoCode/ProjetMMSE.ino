#include <SoftwareSerial.h>
#include <Servo.h>
#include "Wire.h"               // Library for I2C communication
#include "LiquidCrystal_I2C.h"  // Library for LCD
#include <Adafruit_NeoPixel.h>

#define DATA 4
#define NUMPIXELS 1

#define TX 10
#define RX 11
#define STATE 12


Servo servoMoteur;
SoftwareSerial mySoft(10, 11);                              //HC05 TX,RX
LiquidCrystal_I2C lcdI2C = LiquidCrystal_I2C(0x27, 16, 2);  // Change to (0x27,20,4) for 20x4 LCD.
Adafruit_NeoPixel strip(NUMPIXELS, DATA, NEO_GRB + NEO_KHZ800);

char c = ' ';
String buff = "";
String allowed[5];
int index = 1;
bool connected = false;
bool opened = false;
bool canPrint = false;

void setup() {
  // put your setup code here, to run once:
  lcdI2C.init();
  lcdI2C.backlight();

  strip.begin();
  strip.setBrightness(20);

  allowed[0] = "045F07AA686180";
  Serial.begin(9600);
  pinMode(STATE, INPUT);
  mySoft.begin(9600);

  servoMoteur.attach(2);
  servoMoteur.write(0);
  delay(500);
  servoMoteur.write(180);
  delay(500);
  servoMoteur.write(0);
}

void resp(char c) {
  mySoft.write(c);
}

void display(String s) {
  for (int i = 0; i < s.length(); i += 1) {
    Serial.print(s[i]);
  }
}

void proceed(String s) {
  lcdClean();
  if (s == "close") {
    servoMoteur.write(0);
    opened = false;
    resp('0');
  } else if (s.startsWith("add+")) {
    if (index < 5) {
      allowed[index] = s.substring(4);
      index += 1;
      lcd(8);
      led("magenta");
      resp('8');
    } else {
      lcd(9);
      led("orange");
      resp('9');
    }
  } else {
    for (int i = 0; i < index; i += 1) {
      if (s == allowed[i]) {
        servoMoteur.write(90);
        opened = true;
        resp('1');
        return;
      }
    }
    lcd(3);
    led("red");
    resp('3');
  }
}

void blink(String color, int duration) {
  if (color == "blue") {
    strip.setPixelColor(0, 0, 0, 255);
  } else if (color == "green") {
    strip.setPixelColor(0, 0, 255, 0);
  } else if (color == "red") {
    strip.setPixelColor(0, 255, 0, 0);
  } else if (color == "magenta") {
    strip.setPixelColor(0, 255, 0, 150);
  } else if (color == "orange") {
    strip.setPixelColor(0, 255, 60, 0);
  } else {
    strip.setPixelColor(0, 255, 255, 0);
  }
  strip.show();
  delay(duration);

  strip.fill(0x000000);
  strip.show();
  delay(duration);
}

void led(String color) {
  if (opened) {
    //blink green
    strip.setPixelColor(0, 0, 255, 0);
    strip.show();
  } else {
    if (color == "blue") {
      //blink blue
      blink("blue", 200);
    } else if (color == "yellow") {
      //blink yellow
      blink("yellow", 500);
    } else if (color == "magenta") {
      for (int i = 0; i < 3; i += 1) {
        blink("magenta", 400);
      }
      lcdClean();
    } else if (color == "orange") {
      for (int i = 0; i < 6; i += 1) {
        blink("orange", 400);
      }
      lcdClean();
    } else {
      //blink red
      for (int i = 0; i < 10; i += 1) {
        blink("red", 100);
      }
      lcdClean();
    }
  }
}

void lcdClean() {
  lcdI2C.clear();
  lcdI2C.setCursor(0, 0);
  canPrint = true;
}

void lcd(int type) {
  if (!canPrint) {
    return;
  }
  if (opened) {
    //Access granted
    lcdI2C.print("Access Granted");
  } else {
    if (type == 0) {
      //Await connection
      lcdI2C.print("Awaiting");
      lcdI2C.setCursor(1, 1);
      lcdI2C.print("connection...");
    } else if (type == 1) {
      //Await nfc tag
      lcdI2C.print("Awaiting Tag");
    } else if (type == 8) {
      lcdI2C.print("Add Tag");
      lcdI2C.setCursor(5, 1);
      lcdI2C.print("Success");
    } else if (type == 9) {
      lcdI2C.print("Add Tag");
      lcdI2C.setCursor(5, 1);
      lcdI2C.print("Fail !");
    } else {
      //Access denied
      lcdI2C.print("Access Denied");
    }
  }
  canPrint = false;
}

void loop() {
  // put your main code here, to run repeatedly:

  lcdClean();
  while (!connected) {
    if (digitalRead(STATE) == HIGH) {
      connected = true;
    }
    //lcd awaiting
    lcd(0);
    //blink blue
    led("blue");
  }

  lcdClean();
  while (connected) {
    lcd(1);
    led("yellow");

    if (digitalRead(STATE) == LOW) {
      connected = false;
      servoMoteur.write(0);
      opened = false;
    }

    while (mySoft.available()) {
      c = mySoft.read();
      Serial.write(c);

      if (c == '\n') {
        proceed(buff);
        buff = "";
      } else {
        buff += c;
      }
    }
  }
}
