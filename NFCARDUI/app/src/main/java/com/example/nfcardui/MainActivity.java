package com.example.nfcardui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class MainActivity extends AppCompatActivity {

    private final String TAG = "NFC";
    private final List<BluetoothDevice> btDevices = new ArrayList<>();
    private final List<String> devices = new ArrayList<>();
    private final List<String> devicesAddr = new ArrayList<>();
    private final List<Boolean> asked = new ArrayList<>();
    private final byte[] buffer = new byte[1];
    private final Object Lock = new Object();
    private Executor mainExecutor;
    private ScheduledExecutorService bluetoothExecutor;
    private ScheduledExecutorService dialExecutor;
    private volatile BluetoothSocket btSocket;
    private BluetoothDevice btConnected;
    private TextView status;
    private ActivityResultLauncher<Intent> activityResultLauncher;
    private NfcAdapter nfcAdapter = null;
    private PendingIntent nfcPendingIntent = null;
    private boolean NFCEnable = false;
    private boolean BTOk = false;
    private BluetoothAdapter bluetoothAdapter;
    private Button connectBtn;
    private Spinner spinner;
    private InputStream inputStream;
    private OutputStream outputStream;
    private int numBytes;

    private Button addBtn;
    private Dialog addDial;
    private Dialog masterDial;
    private boolean addBool = false;
    private boolean addMaster = false;

    private static String ByteArrayToHexString(byte[] inarray) {
        int i, j, in;
        String[] hex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};
        StringBuilder out = new StringBuilder();

        for (j = 0; j < inarray.length; ++j) {
            in = (int) inarray[j] & 0xff;
            i = (in >> 4) & 0x0f;
            out.append(hex[i]);
            i = in & 0x0f;
            out.append(hex[i]);
        }
        return out.toString();
    }

    private void displayAlert(String title, String msg, turnOn type) {
        AlertDialog.Builder alertBox = new AlertDialog.Builder(this);
        alertBox.setTitle(title);
        alertBox.setMessage(msg);
        if (type == turnOn.BT || type == turnOn.NFC) {
            alertBox.setPositiveButton("Turn On", (dialog, which) -> {
                Intent intent;
                if (type == turnOn.BT) {
                    intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                } else {
                    intent = new Intent(Settings.ACTION_NFC_SETTINGS);
                }
                activityResultLauncher.launch(intent);
                asked.set(type.ordinal(), false);
            });
        }
        alertBox.setNegativeButton("Close", (dialog, which) -> {
            if (type == turnOn.BT || type == turnOn.NFC) {
                asked.set(type.ordinal(), false);
            }
        });
        if (type == turnOn.BT || type == turnOn.NFC) {
            alertBox.setOnCancelListener(dialog -> asked.set(type.ordinal(), false));
            alertBox.setOnDismissListener(dialog -> asked.set(type.ordinal(), false));
        }
        alertBox.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bluetoothExecutor.shutdown();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainExecutor = ContextCompat.getMainExecutor(this);
        bluetoothExecutor = Executors.newSingleThreadScheduledExecutor();

        status = findViewById(R.id.BTStatus);
        spinner = findViewById(R.id.BTList);
        connectBtn = findViewById(R.id.connect);
        addBtn = findViewById(R.id.addUser);

        addBtn.setOnClickListener(v -> masterTag());

        for (int i = 0; i < turnOn.values().length; i++) {
            asked.add(false);
        }

        activityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            if (result.getResultCode() == Activity.RESULT_OK) {
                Log.e("Activity result", "OK");
                // There are no request codes
                Intent data = result.getData();
            }
        });

        //Check for BT adapter
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            // Device doesn't support Bluetooth
            errDial("No Bluetooth !", "No bluetooth available on this device.\nYou can't use this app.");
        }

        BTisOn();

        //Check for nfc adapter
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            errDial("No NFC !", "No NFC adapter available on this device.\nYou can't use this app.");
        }

        NFCisOn();
    }

    private void errDial(String title, String text) {
        Dialog builder = new Dialog(this);
        builder.setOnDismissListener(dialogInterface -> this.onDestroy());
        builder.setTitle(title);
        builder.setOnCancelListener(dialog -> this.onDestroy());
        TextView textView = new TextView(this);
        textView.setText(text);
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(getResources().getColor(R.color.red, getTheme()));
        builder.addContentView(textView, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();
    }

    private void NFCisOn() {
        if (!nfcAdapter.isEnabled() && !asked.get(turnOn.NFC.ordinal())) {
            asked.set(turnOn.NFC.ordinal(), true);
            displayAlert("Turn On NFC Reader", "NFC Reader needs to be turn on", turnOn.NFC);
        }
    }

    private void BTisOn() {
        //Check if BT adapter Enable
        if (!bluetoothAdapter.isEnabled() && !asked.get(turnOn.NFC.ordinal())) {
            asked.set(turnOn.NFC.ordinal(), true);
            if (ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.BLUETOOTH_CONNECT) == PackageManager.PERMISSION_DENIED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.BLUETOOTH_CONNECT}, 2);
                }
            }
            displayAlert("Turn On Bluetooth", "Bluetooth needs to be turn on", turnOn.BT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        while (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.BLUETOOTH_CONNECT}, 2);
        }

        BTisOn();
        NFCisOn();

        if (nfcAdapter.isEnabled()) {
            Log.v(TAG, "NFC ok");
            NFCEnable = true;
        }

        if (bluetoothAdapter.isEnabled()) {
            Log.v(TAG, "BT ok");
            BTOk = true;
        }

        if (NFCEnable) {
            nfcPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), PendingIntent.FLAG_MUTABLE);
            nfcAdapter.enableForegroundDispatch(this, nfcPendingIntent, null, null);
        }


        if (BTOk) {

            if (btSocket == null || !btSocket.isConnected()) {
                status.setText(R.string.awaiting_connection);
                status.setTextColor(getColor(R.color.yellow));
            }
            Log.d(TAG, String.valueOf(btSocket == null));

            Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

            if (pairedDevices.size() > 0) {
                // There are paired devices. Get the name and address of each paired device.
                for (BluetoothDevice device : pairedDevices) {
                    btDevices.add(device);
                    String deviceName = device.getName();
                    String deviceHardwareAddress = device.getAddress(); // MAC address
                    devices.add(deviceName);
                    devicesAddr.add(deviceHardwareAddress);
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item, devices);

                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
            }

            connectBtn.setEnabled(true);
            connectBtn.setOnClickListener(v -> {
                if (btSocket == null) {
                    int index = spinner.getSelectedItemPosition();
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    btConnected = btDevices.get(index);
                    Toast.makeText(getApplicationContext(), btConnected.getName(), Toast.LENGTH_SHORT).show();

                    bluetoothExecutor.execute(new ConnectThread(btConnected));

                    Log.e(TAG, "DEBUG " + bluetoothExecutor.toString());


                } else {
                    Log.e(TAG, "DEBUG " + bluetoothExecutor.toString());
                    disconnectBT();
                }
            });
        } else {
            connectBtn.setEnabled(false);
        }
    }

    private void disconnectBT() {
        try {
            btSocket.close();
            resetSocStream();
            mainExecutor.execute(() -> {
                status.setText(getText(R.string.awaiting_connection));
                status.setTextColor(getColor(R.color.yellow));
                connectBtn.setText(R.string.connect);
                connectBtn.setTextColor(getColor(R.color.titleBand));
                addBtn.setEnabled(false);
            });
        } catch (IOException e) {
            Log.e(TAG, "Could not close the client socket", e);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (NFCEnable) {
            nfcAdapter.disableForegroundDispatch(this);
        }
    }


    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String action = intent.getAction();

        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action) || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action) || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Log.v(TAG, "action := " + intent.getAction());

            if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
                Parcelable[] tagFromIntent = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
                Log.d(TAG, "Placable : " + Arrays.toString(tagFromIntent));

            } else if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {
                Parcelable[] tagFromIntent = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_TAG);
                Log.d(TAG, "Placable : " + Arrays.toString(tagFromIntent));
            } else {
                Tag tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                Log.d(TAG, "Placable : " + tagFromIntent);

                StringBuilder type = new StringBuilder();
                type.append("Type :");

                for (String tech : Objects.requireNonNull(tagFromIntent).getTechList()) {
                    Log.v(TAG, "type := " + tech);
                    if (tech.equals("android.nfc.tech.NfcA")) {
                        type.append(" NFC-A ISO 14443-3A");
                    }
                    if (tech.equals("android.nfc.tech.NfcB")) {
                        type.append(" NFC-B ISO 14443-3B");
                    }
                    if (tech.equals("android.nfc.tech.NfcV")) {
                        type.append(" NFC-V ISO 15693");
                    }
                    if (tech.equals("android.nfc.tech.IsoDep")) {
                        type.append(" ISO-DEP ISO 14443-4");
                    }
                    if (tech.equals("android.nfc.tech.MifareClassic")) {
                        Toast.makeText(getApplicationContext(), "Badge Mifare", Toast.LENGTH_LONG).show();
                        type.append(" MifareClassic");
                        String informations = lireMifare(tagFromIntent);
//                        texteMessageDonneesNFC.setText(informations);
                    }
                }
                if (type.length() > 0) {
//                    texteMessageTypeNFC.setText(type.toString());
                    String uuid = ByteArrayToHexString(Objects.requireNonNull(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID)));
//                    texteMessageIdNFC.setText(String.format("UUID : %s", uuid));
                    Log.d(TAG, "uuid : " + uuid);

                    ASPIC(uuid);

                }
            }
        }
    }

    private void ASPIC(String uuid) {
        String myTag = "045F07AA686180";

        if (addBool) {
            writeStream("add+" + uuid);
            addBool = false;
            addDial.dismiss();
        } else if (addMaster) {
            if (Objects.equals(uuid, myTag)) {
                addDial.dismiss();
                displayAdd();
            } else {
                displayAlert("Authorization Denied", "You are not allowed to perform this action !", turnOn.NULL);
            }
        } else {
            writeStream(uuid);
        }

        Toast.makeText(getApplicationContext(), "Tag forwarded", Toast.LENGTH_SHORT).show();
    }

    private String lireMifare(Tag tag) {
        MifareClassic mif = MifareClassic.get(tag);

        StringBuilder informations = new StringBuilder();
        int type = mif.getType();
        switch (type) {
            case MifareClassic.TYPE_CLASSIC:
                informations.append("Type : MIFARE Classic\n");
                break;
            case MifareClassic.TYPE_PLUS:
                informations.append("Type : MIFARE Plus\n");
                break;
            case MifareClassic.TYPE_PRO:
                informations.append("Type : MIFARE Pro\n");
                break;
            case MifareClassic.TYPE_UNKNOWN:
                informations.append("Type : MIFARE Classic compatible\n");
                break;
        }

        int size = mif.getSize();
        informations.append("Taille : ").append(size).append("\n");

        int nbSecteurs = mif.getSectorCount();
        informations.append("Nb secteurs : ").append(nbSecteurs).append("\n");

        int nbBlocs = mif.getBlockCount();
        informations.append("Nb blocs : ").append(nbBlocs).append("\n");

        try {
            mif.connect();
            if (mif.isConnected()) {
                for (int i = 0; i < nbSecteurs; i++) {
                    boolean isAuthenticated = false;

                    if (mif.authenticateSectorWithKeyA(i, MifareClassic.KEY_MIFARE_APPLICATION_DIRECTORY)) {
                        isAuthenticated = true;
                    } else if (mif.authenticateSectorWithKeyA(i, MifareClassic.KEY_DEFAULT)) {
                        isAuthenticated = true;
                    } else if (mif.authenticateSectorWithKeyA(i, MifareClassic.KEY_NFC_FORUM)) {
                        isAuthenticated = true;
                    } else {
                        Log.d("TAG", "Autorisation := refusée");
                    }

                    if (isAuthenticated) {
                        int index = mif.sectorToBlock(i);
                        byte[] block = mif.readBlock(index);
                        String s_block = MainActivity.ByteArrayToHexString(block);
                        if (index < 10)
                            informations.append("bloc ").append(index).append("  : ").append(s_block).append("\n");
                        else
                            informations.append("bloc ").append(index).append(" : ").append(s_block).append("\n");
                    } else informations.append("Aucune autorisation\n");
                }
            }
            mif.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return informations.toString();
    }


    public void resetSocStream() {
        btSocket = null;
        inputStream = null;
        outputStream = null;
    }

    private void writeStream(String s) {
        byte[] bytes = (s + "\n").getBytes(StandardCharsets.UTF_8);
        try {
            outputStream.write(bytes);
        } catch (IOException e) {
            Log.e(TAG, "Error occurred when sending data", e);
            Toast.makeText(getApplicationContext(), "Couldn't send data to the other device", Toast.LENGTH_LONG).show();
            throw new RuntimeException(e);
        }
    }

    @Nullable
    private String readStream() {
        try {
            numBytes = inputStream.read(buffer);
            if (numBytes > 0) {
                Log.i(TAG, "bytes received : " + numBytes);
                return new String(buffer, StandardCharsets.UTF_8);
            }
        } catch (IOException e) {
            Log.d(TAG, "Input stream was disconnected", e);
            Toast.makeText(getApplicationContext(), "Input stream was disconnected", Toast.LENGTH_LONG).show();
            throw new RuntimeException(e);
        }
        return null;
    }

    private void displayNotif() {
        AlertDialog.Builder alertBox = new AlertDialog.Builder(this);
        alertBox.setTitle("Access Granted");
        alertBox.setMessage("The lock is open.\nPress the following button to close it again.");
        alertBox.setPositiveButton("Close lock", (dialog, which) -> {
            writeStream("close");
        });

        Dialog dialog = alertBox.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void masterTag() {
        AlertDialog.Builder alertBox = new AlertDialog.Builder(this);
        alertBox.setTitle("Authorization Needed");
        alertBox.setMessage("Please pass the master Tag.");
        alertBox.setNegativeButton("Cancel", (dialog, which) -> {
            addMaster = false;
            addDial.dismiss();
        });
        addDial = alertBox.create();
        addDial.setCanceledOnTouchOutside(false);
        addMaster = true;
        addDial.show();
    }

    private void displayAdd() {
        AlertDialog.Builder alertBox = new AlertDialog.Builder(this);
        alertBox.setTitle("Add User");
        alertBox.setMessage("You can now add a user by scanning the associated NFC tag.");
        alertBox.setNegativeButton("Cancel", (dialog, which) -> {
            addBool = false;
            addDial.dismiss();
        });
        addDial = alertBox.create();
        addDial.setCanceledOnTouchOutside(false);
        addMaster = false;
        addBool = true;
        addDial.show();
    }

    private enum turnOn {
        BT, NFC, NULL
    }

    private interface messageCode {
        public static final String DOOR_CLOSE = "0";
        public static final String DOOR_OPEN = "1";
        public static final String NO_ACCESS = "3";
        public static final String ADD_OK = "8";
        public static final String ADD_FULL = "9";
    }

    private class ConnectThread extends Thread {
        private final BluetoothDevice mmDevice;
        private BluetoothSocket mmSocket;

        public ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket
            // because mmSocket is final.
            BluetoothSocket tmp = null;
            mmDevice = device;

            try {
                // Get a BluetoothSocket to connect with the given BluetoothDevice.
                // MY_UUID is the app's UUID string, also used in the server code.
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.BLUETOOTH_CONNECT}, 2);
                    return;
                }
                tmp = mmDevice.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
            } catch (IOException e) {
                Log.e(TAG, "Socket's create() method failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it otherwise slows down the connection.
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.BLUETOOTH_SCAN}, 2);
                return;
            }
            bluetoothAdapter.cancelDiscovery();

            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                mmSocket.connect();
            } catch (IOException connectException) {
                // Unable to connect; close the socket and return.
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.e(TAG, "Could not close the client socket", closeException);
                }
                return;
            }

            // The connection attempt succeeded. Perform work associated with
            // the connection in a separate thread.

            try {
                inputStream = mmSocket.getInputStream();
                outputStream = mmSocket.getOutputStream();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            btSocket = mmSocket;

            if (btSocket.isConnected()) {
                mainExecutor.execute(() -> {
                    status.setText(String.format("Connected to %s", btConnected.getName()));
                    status.setTextColor(getColor(R.color.green));
                    connectBtn.setText(R.string.disconnect);
                    connectBtn.setTextColor(getColor(R.color.red));
                    addBtn.setEnabled(true);
                });
                dialExecutor = Executors.newSingleThreadScheduledExecutor();
                dialExecutor.execute(new ConnectedThread());
            }
        }
    }

    private class ConnectedThread extends Thread {
        public void run() {
            while (btSocket != null) {
                String s = readStream();
                if (s != null) {
                    mainExecutor.execute(() -> {
                        Log.d(TAG, s);
                        switch (s) {
                            case messageCode.DOOR_OPEN:
                                displayNotif();
                                break;
                            case messageCode.DOOR_CLOSE:
                                Toast.makeText(getApplicationContext(), "Lock Successful", Toast.LENGTH_SHORT).show();
                                break;
                            case messageCode.ADD_OK:
                                //TODO
                                Toast.makeText(getApplicationContext(), "Tag successfully added", Toast.LENGTH_SHORT).show();
                                break;
                            case messageCode.ADD_FULL:
                                //TODO
                                break;
                            case messageCode.NO_ACCESS:
                                Toast.makeText(getApplicationContext(), "You're not allowed to proceed", Toast.LENGTH_LONG).show();
                                break;
                            default:
                                break;
                        }
                    });
                }
            }
        }
    }
}